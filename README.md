## Versión de Ruby
Ruby: 3.0.2p107

## Ejecución del programa
Par ejecutar el programa es necesario colocarse en la carpeta principal y ejecutar el siguiente comando:

```js
cd src
ruby main.rb
```

Al ejecutar el comando nos mostrará el listado de productos disponibles junto con las ofertas que tiene cada producto.

Para rellenar la cesta el programa nos solicitará que introduzcamos productos, en el caso de no introducirlos correctamente nos mostrará el error "Error de formato: los productos deben introducirse por el código p.e. 'PR1,PR2'", donde se indica el formato a utilizar. En el caso de introducir correctamente los productos se realizará una agrupación de comunes, es decir, agrupará los productos bajo el formato {"PR1" => ["PR1","pr1"], "PR2" => ["pr2", "Pr2"]}.

El filtrado permite meter productos indistintamente de usar mayúsculas o minúsculas.

Una vez agrupados los productos introducidos revisa si existen los productos y si tienen asignada una oferta, en el caso de tener oferta se comprobará el precio final del conjunto de productos y se sumará al total

Por último se mostrarán los productos introducidos y su precio final en este formato.

```js
Productos disponibles:
==================================================
--------------------------------------------------
Código: GR1
Nombre: Green tea
Precio: £3.11
Oferta: Descuento compra uno y te llevarás uno gratis
--------------------------------------------------
--------------------------------------------------
Código: SR1
Nombre: Strawberries
Precio: £5.00
Oferta: Descuento compra 3 o más y te restaremos al precio de uno £0.50
--------------------------------------------------
--------------------------------------------------
Código: CF1
Nombre: Coffee
Precio: £11.23
Oferta: Sin descuentos
--------------------------------------------------

==================================================
Productos de cesta
==================================================
Introduce productos: gr1

Productos en cesta: gr1
Total de cesta: £3.11
```

## Test unitarios
Para lanzar los test unitarios es necesario colocarse en la carpeta principal y ejecutar el siguiente comando:

```js
cd src
ruby test.rb
```

