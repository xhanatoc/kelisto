#!/usr/bin/env ruby
$LOAD_PATH << './controllers'
require "checkout.rb"
require "test/unit"
 
class TestCheckout < Test::Unit::TestCase
    def setup
        @co = Checkout.new()
    end

    def test_simple1
        @co.scan("GR1,SR1,GR1,GR1,CF1".split(','))
        assert_equal(22.45, @co.total )
    end

    def test_simple2
        @co.scan("GR1,GR1".split(','))
        assert_equal(3.11, @co.total )
    end

    def test_simple3
        @co.scan("SR1,SR1,GR1,SR1".split(','))
        assert_equal(16.61, @co.total )
    end
 
    def test_simple4
        @co.scan("SR1".split(','))
        assert_equal(5.00, @co.total )
    end

    def test_simple5
        @co.scan("GR1".split(','))
        assert_equal(3.11, @co.total )
    end

    def test_simple6
        @co.scan("CF1".split(','))
        assert_equal(11.23, @co.total )
    end
end