class Offert
    KIND = [:buy_one_free_one, :buy_three_or_more].freeze

    attr_accessor :description, :descount

    def initialize(_descount)
        if KIND.include?(_descount.to_s.to_sym)
            @descount = _descount.to_s.to_sym
            case _descount.to_s
            when "buy_one_free_one"
                @description = "Descuento compra uno y te llevarás uno gratis"
            when "buy_three_or_more"
                @description = "Descuento compra 3 o más y te restaremos al precio de uno £0.50"
            end
        else
            @description = "Sin descuentos"
            @descount = nil
        end
    end

    def to_string
        "#{@description}"
    end

    def self.proccess_offert(price,num, kind)
        if KIND.include?(kind.to_s.to_sym)
            case kind.to_s
            when "buy_one_free_one"
                num === 1 ? price : (price * ((num - (num%2 == 0 ? 0 : 1))/2) + (num%2 == 0 ? 0 : price))
            when "buy_three_or_more"
                num >= 3 ? (price - 0.5) * num : price * num
            else
                price * num
            end
        else
           price * num
        end
    end

    
end