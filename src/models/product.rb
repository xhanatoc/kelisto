require "offert.rb"
class Product
    attr_accessor :item_code, :name, :price, :offert
   
    def initialize(_item_code,_name, _price, _offert)
        @item_code=_item_code
        @name= _name
        @price= _price
        @offert = _offert
    end

    def to_string
        "Código: #{@item_code}\nNombre: #{@name}\nPrecio: #{@price}\nOferta: #{@offert.to_string}"
    end

    def get_price_value
        @price.gsub(/[a-zA-Z£]*/,'').to_f
    end
end