#!/usr/bin/env ruby
    $LOAD_PATH << './controllers'
    require "checkout.rb"

    CO = Checkout.new()
    puts "="*50
    puts "Productos disponibles:"
    puts "="*50
    CO.showProducts

    puts ""*3
    puts "="*50
    puts "Productos de cesta"
    puts "="*50
    print "Introduce productos: "
    basket = gets

    if basket.match(/^[a-zA-Z0-9]+(,+[a-zA-Z0-9]+)*$/)
        puts ""*3
        puts "Productos en cesta: #{basket}"
        products_code = basket.split(',')
        CO.scan(products_code)
        PRICE = CO.total

        puts "Total de cesta: £#{ PRICE }"
    else
        puts "Error de formato: los productos deben introducirse por el código p.e. 'PR1,PR2'"
    end