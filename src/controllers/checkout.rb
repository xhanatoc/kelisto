$LOAD_PATH << './models'
require "product.rb"
require "offert.rb"

class Checkout
    attr_accessor :products, :total

    def initialize()
        @products = []
        @products.push( Product.new("GR1", "Green tea", "£3.11", Offert.new("buy_one_free_one")))
        @products.push( Product.new("SR1", "Strawberries", "£5.00", Offert.new(:buy_three_or_more)))
        @products.push( Product.new("CF1", "Coffee", "£11.23", Offert.new(nil)))
        @total = 0
    end

    def scan(item)
        return if item.nil?
        list_of_products = item.group_by {|i| i.strip.upcase}
        list_of_products.each do |k,v|
            aux = @products.find {|p| "#{p.item_code}".strip.upcase === "#{k}".strip.upcase}
            if !aux.nil?
                price = Offert.proccess_offert(aux.get_price_value,v.count,aux.offert.descount)
                @total += price 
            end
        end
    end

    def showProducts
        @products.each do |p|
            puts "-"*50
            puts p.to_string
            puts "-"*50
        end
    end

end
